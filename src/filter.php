<?php

class Filter
{
    const varChar = '?';
    const evalDefault = false;

    public $expressions;
    public $operator;
    public $var;

    function __construct(string $expression, $var = null)
    {
        $this->var = $var;

        $expression = trim($expression);
        if(preg_match("/^\([&!|].*\)$/", $expression) === 1){
            $this->expressions = self::getInnerExpressions($expression);
            $this->operator = $expression[1];

        }
        elseif(preg_match("/^\(.*=.*\)$/", $expression) === 1){
            $expression = substr($expression, 1, -1);
            preg_match("/[<>~]?=/", $expression, $match);
            $this->expressions = explode($match[0], $expression);
            $this->operator = $match[0];
        }
    }

    function eval(){
        switch ($this->operator){
            case "&":
                foreach ($this->expressions as $expression){
                    if(!$expression->eval()){
                        return false;
                    }
                }
                return true;
                break;
            case "|":
                foreach ($this->expressions as $expression){
                    if($expression->eval()){
                        return true;
                    }
                }
                return false;
                break;
            case "!":
                return !$this->expressions[0]->eval();
                break;
            case "=":
                return $this->getValue($this->expressions[0]) == $this->getValue($this->expressions[1]);
                break;
            case ">=":
                return $this->getValue($this->expressions[0]) >= $this->getValue($this->expressions[1]);
                break;
            case "<=":
                return $this->getValue($this->expressions[0]) <= $this->getValue($this->expressions[1]);
                break;
        }
        return self::evalDefault;
    }

    function getValue($expression){
        if($expression[0] == self::varChar){
            try {
                if (is_array($this->var)) {
                    $expression = $this->var[substr($expression, 1)];
                } else {
                    $expression = $this->var->${$expression};
                }
            } catch (Exception $e){
                return "";
            }
        }
        if(is_numeric($expression)){
            return $expression + 0;
        }
        if($expression == "true"){
            return true;
        }
        if($expression == "false"){
            return false;
        }
        return $expression;
    }

    function getInnerExpressions($expression){
        $y = 0;
        $b = 0;
        $expressions = array();
        $expression = substr($expression, 2, -1);
        foreach (str_split($expression) as $i => $char){
            switch ($char){
                case '(':
                    if($y == 0) $b = $i;
                    $y++;
                    break;
                case ')':
                    $y--;
                    if($y == 0) {
                        $expressions[] = new Filter(substr($expression, $b, $i-$b+1), $this->var);
                    }
                    break;
            }

        }
        return $expressions;
    }
}
